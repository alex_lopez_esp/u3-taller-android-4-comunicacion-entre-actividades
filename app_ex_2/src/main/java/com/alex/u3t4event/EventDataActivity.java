package com.alex.u3t4event;


import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.location.GnssAntennaInfo;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.Timer;


public class EventDataActivity extends AppCompatActivity implements View.OnClickListener,RadioGroup.OnCheckedChangeListener {
    private TextView tvEventName;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private Button chooseDay;
    private Button chooseHour;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;
    private String priority="Normal";
    private TextView textViewNuevo;
    private EditText editTextNuevo;
    private TextView ponerFecha;
    private TextView ponerHora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);
        setUI();

        Bundle inputData=getIntent().getExtras();

        tvEventName.setText(inputData.getString("EventName"));


    }

    private void setUI() {
        tvEventName=(TextView) findViewById(R.id.tvEventName);
        rgPriority=(RadioGroup) findViewById(R.id.radioGroup);

        rgPriority.check(R.id.rbNormal);



        btAccept=(Button) findViewById(R.id.btAccept);
        btCancel=(Button) findViewById(R.id.btCancel);
        textViewNuevo=findViewById(R.id.textView2);
        editTextNuevo=findViewById(R.id.editTextTextPersonName);
        chooseHour=findViewById(R.id.elegirHora);
        chooseDay=findViewById(R.id.elegirDia);
        ponerFecha=findViewById(R.id.ponerFecha);
        ponerHora=findViewById(R.id.ponerHora);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        chooseDay.setOnClickListener(this);
        chooseHour.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
    }


    @SuppressLint({"NonConstantResourceId", "SetTextI18n"})
    @Override
    public void onClick(View view) {

        Intent activityResult=new Intent();
        Bundle eventData=new Bundle();
        switch (view.getId()){
            case R.id.elegirDia://TODO CREACION DEL FRAGMENT DE DATEPICKER
                final  Calendar calendar=Calendar.getInstance();
                int mYear = calendar.get(Calendar.YEAR);
                int mMonth = calendar.get(Calendar.MONTH);
                int mDay = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        ponerFecha.setText(dayOfMonth + "/"+ (month + 1) + "/" + year);
                    }
                }
                ,mYear,mMonth,mDay);
                datePickerDialog.show();
                break;


            case R.id.elegirHora://TODO CREACION DEL FRAGMENT DE TIMEPICKER
                TimePickerDialog picker;
                final Calendar calendar1=Calendar.getInstance();
                int hour=calendar1.get(Calendar.HOUR_OF_DAY);
                int minutes=calendar1.get(Calendar.MINUTE);
                picker=new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        ponerHora.setText(hourOfDay+":"+minute);
                    }
                },hour,minutes,true);
                picker.show();

                break;


            case R.id.btAccept:
                String[] month = getResources().getStringArray(R.array.Meses);
                String[] fecha=ponerFecha.getText().toString().split("/");
                String prueba=(fecha[0]+" "+month[Integer.parseInt(fecha[1])-1]+" "+fecha[2]);

                eventData.putString("EventData","PLACE:"+editTextNuevo.getText()+"\nPriority: "+priority+"\n" +"DATE: "+prueba+"\nHOUR: "+ ponerHora.getText().toString());
                //eventData.putString("EventData","PLACE:"+editTextNuevo.getText()+"\nPriority: "+priority+"\n" +"DATE: "+fecha[0]+" "+month[Integer.parseInt(fecha[1])]+" "+fecha[2]+"\nHOUR: "+ ponerHora.getText().toString());



                setResult(RESULT_OK,activityResult);
                activityResult.putExtras(eventData);
                finish();
                break;
            case R.id.btCancel:
                eventData.putString("EventData","");
                setResult(RESULT_CANCELED,activityResult);
                finish();
                break;
        }


    }
    @SuppressLint("NonConstantResourceId")
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i){
            case R.id.rbLow:
                priority="Low";
                break;
            case R.id.rbNormal:
                priority="Normal";
                break;
            case R.id.rbHigh:
                priority="High";
                break;
        }

    }


}