package com.alex.u3t4event;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener,RadioGroup.OnCheckedChangeListener {
    private TextView tvEventName;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;
    private String priority="Normal";
    private TextView textViewNuevo;
    private EditText editTextNuevo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);
        setUI();

        Bundle inputData=getIntent().getExtras();

        tvEventName.setText(inputData.getString("EventName"));


    }

    private void setUI() {
        tvEventName=(TextView) findViewById(R.id.tvEventName);
        rgPriority=(RadioGroup) findViewById(R.id.radioGroup);

        rgPriority.check(R.id.rbNormal);

        dpDate=(DatePicker) findViewById(R.id.dpDate);
        tpTime=(TimePicker) findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAccept=(Button) findViewById(R.id.btAccept);
        btCancel=(Button) findViewById(R.id.btCancel);
        textViewNuevo=findViewById(R.id.textView2);
        editTextNuevo=findViewById(R.id.editTextTextPersonName);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {

        Intent activityResult=new Intent();
        Bundle eventData=new Bundle();
        switch (view.getId()){
            case R.id.btAccept:
                String[] month = getResources().getStringArray(R.array.Meses);//TODO Creacion de el array de meses
                eventData.putString("EventData","PLACE:"+editTextNuevo.getText()+"\nPriority: "+priority+"\n"
                        +"DATE: "+dpDate.getDayOfMonth()+" "+month[dpDate.getMonth()]+" "+dpDate.getYear()+"\nHOUR: "+ tpTime.getHour()+":"+tpTime.getMinute());//Todo para

                setResult(RESULT_OK,activityResult);

                break;
            case R.id.btCancel://Todo cuando pulsamos el boton cancel
                eventData.putString("EventData","");
                setResult(RESULT_CANCELED,activityResult);
                break;
        }
        activityResult.putExtras(eventData);
        finish();
    }
    @SuppressLint("NonConstantResourceId")
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i){
            case R.id.rbLow:
                priority="Low";
                break;
            case R.id.rbNormal:
                priority="Normal";
                break;
            case R.id.rbHigh:
                priority="High";
                break;
        }

    }


}